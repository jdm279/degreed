﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icanhazdadjoke;
using Icanhazdadjoke.Interfaces;
using System.Linq;
using System.Text.RegularExpressions;

namespace degreedCodeChallenge
{
    class Program
    {
        static IIcanhazdadjokeRepository repo { get; set; }
        static void Main(string[] args)
        {
            SetDependencies();
            DisplayMainMenu();
        }

        static void SetDependencies()
        {
            using (var kernel = new StandardKernel())
            {
                DependencyRegistration.Register(kernel);
                repo = kernel.Get<IIcanhazdadjokeRepository>();
            }
        }

        static void DisplayMainMenu()
        {
            Console.Clear();
            Write("icanhazdadjoke");
            Write("(1) Random Joke");
            Write("(2) Search For Joke");
            Write("-- Leave blank and hit enter to exit --");
            ProcessMainMenuResponse();
        }

        static void ProcessMainMenuResponse()
        {
            string answer = Read();
            if (string.IsNullOrEmpty(answer))
            {
                return;
            }
            else if (int.TryParse(answer, out int response))
            {
                switch (response)
                {
                    case 1: DisplayRandomJoke(); break;
                    case 2: SearchJokeMenu(); break;
                    default: Write("Invalid Response"); break;
                }
            }
            else
            {
                Write("Invalid Response");
            }
            Write("Hit enter to return to Main Menu");
            Read();
            DisplayMainMenu();
        }

        private static void SearchJokeMenu()
        {
            Write("Search Term:");
            string terms = Read();
            IEnumerable<string> jokes = repo.SearchJokes(terms);
            DisplaySortedJokesWithSearchTermsMarked(terms, jokes);
        }

        private static void DisplaySortedJokesWithSearchTermsMarked(string terms, IEnumerable<string> jokes)
        {
            int shortLength = 10,
                mediumLength = 20;

            Write($"Short(< {shortLength} words)");
            jokes.Where(x => x.Split(' ').Count() < shortLength)
                .ToList().ForEach(joke => DisplayHighlightTerm(terms, joke));
            
            Write();
            Write();

            Write($"Medium(< {mediumLength} words)");
            jokes.Where(x => x.Split(' ').Count() < mediumLength && x.Length >= shortLength)
                .ToList().ForEach(joke => DisplayHighlightTerm(terms, joke));

            Write();
            Write();
                
            Write($"Long(>= {mediumLength} words)");
            jokes.Where(x => x.Split(' ').Count() >= mediumLength)
                     .ToList().ForEach(joke => DisplayHighlightTerm(terms, joke));

            Write();
            Write();
        }

        private static void DisplayHighlightTerm(string terms, string joke)
        {
            if (string.IsNullOrEmpty(terms))
            {
                Write(joke);
            }
            else
            {
                terms.Split(' ').ToList().ForEach(x =>
                {
                    string term = Regex.Escape(x);
                    joke = Regex.Replace(joke, $"{x}", $"|{x}|", RegexOptions.IgnoreCase);
                });
                Write(joke);
            }
        }

        private static void DisplayRandomJoke()
        {
            Write("Getting Random Joke");
            string joke = repo.GetRandomJoke();
            Clear();
            Write(joke);
            Write();
            Write();

        }

        static void Write(string message = null)
        {
            Console.WriteLine(message ?? "");
        }
        static void Clear()
        {
            Console.Clear();
        }
        static string Read()
        {
            return Console.ReadLine();
        }
    }
}
