﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icanhazdadjoke.Interfaces;

namespace Icanhazdadjoke
{
    /// <summary>
    /// Provides default implementations
    /// </summary>
    public class IcanhazdadjokeFactory
    {
        public static IRequestData GetImplementationForIRequestData()
        {
            return new WebRequestManager();
        }
        public static IIcanhazdadjokeRepository GetImplementationForIIcanhazdadjokeRepository(IRequestData data)
        {
            return new IcanhazdadjokeRepository(data);
        }
    }
}
