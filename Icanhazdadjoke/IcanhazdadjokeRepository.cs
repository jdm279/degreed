﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Icanhazdadjoke.Interfaces;
using Icanhazdadjoke.POCO;

namespace Icanhazdadjoke
{
    public class IcanhazdadjokeRepository : Interfaces.IIcanhazdadjokeRepository
    {
        private IRequestData dataManager { get; set; }
        public IcanhazdadjokeRepository(IRequestData data)
        {
            dataManager = data;
        }
        public string GetRandomJoke()
        {
            string url = "https://icanhazdadjoke.com/";
            JokeResponse jokeResponse = null;
            dataManager.Get(url, (wc, getData) =>
            {
                wc.Headers.Add(System.Net.HttpRequestHeader.Accept, "application/json");

                string response = getData.Invoke();
                jokeResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<JokeResponse>(response);

                if (jokeResponse.Status != 200)
                {
                    throw new Exception($"Invalid Response: {jokeResponse.Status}");
                }
            });
            return jokeResponse.Joke;
        }

        public IEnumerable<string> SearchJokes(string search, int page = 1, int limit = 30)
        {
            string url = "https://icanhazdadjoke.com/search";
            JokeSearchResponse jokeResponse = null;
            dataManager.Get(url, (wc, getData) =>
            {
                if (!string.IsNullOrEmpty(search))
                {
                    wc.QueryString.Add("term", search);
                }
                wc.QueryString.Add("page", page.ToString());
                wc.QueryString.Add("limit", limit.ToString());
                wc.Headers.Add(System.Net.HttpRequestHeader.Accept, "application/json");

                string response = getData.Invoke();
                jokeResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<JokeSearchResponse>(response);

                if (jokeResponse.Status != 200)
                {
                    throw new Exception($"Invalid Response: {jokeResponse.Status}");
                }
            });
            return jokeResponse.Results.Select(x=>x.Joke);
        }
    }
}
