﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Icanhazdadjoke.Interfaces
{
    /// <summary>
    /// Manages data retrieval
    /// </summary>
    public interface IRequestData
    {
        /// <summary>
        /// Provides a way to easily send and customize GET data
        /// </summary>
        void Get(string url, Action<WebClient, Func<string>> callback, int retryAttempts = 0);
    }
}
