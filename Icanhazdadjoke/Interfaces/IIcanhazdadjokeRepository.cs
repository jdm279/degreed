﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icanhazdadjoke.Interfaces
{
    /// <summary>
    /// Repository for retrieving jokes
    /// </summary>
    public interface IIcanhazdadjokeRepository
    {
        /// <summary>
        /// Returns a single random joke
        /// </summary>
        string GetRandomJoke();
        /// <summary>
        /// Returns x amount of jokes based on search terms that are restricted by limit and paging
        /// </summary>
        /// <param name="search"></param>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        IEnumerable<string> SearchJokes(string search, int page = 1, int limit = 30);
    }
}
