﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Icanhazdadjoke.Interfaces;

namespace Icanhazdadjoke
{
    internal class WebRequestManager : IRequestData
    {
        /// <summary>
        /// Attempts to GET with exception and retry
        /// </summary>
        public void Get(string url, Action<WebClient, Func<string>> callback, int retryAttempts = 0)
        {
            retryAttempts++; // Add one, because the first isn't really a retry.
            Exception exception = null;
            while (retryAttempts-- >= 0)
            {
                try
                {
                    exception = null;
                    using (WebClient wc = new WebClient())
                    {
                        callback.Invoke(wc, () =>
                        {
                            return wc.DownloadString(url);
                        });
                    }
                    break;
                }
                catch (Exception e)
                {
                    Console.WriteLine("-- Exception ---");
                    Console.WriteLine(e.Message);

                    // TODO: Handle the excption
                    exception = e;
                }
                finally
                {
                    if(exception != null)
                    {
                        throw exception;
                    }
                }
            }

        }
    }
}
