﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Icanhazdadjoke.POCO
{
    class JokeSearchResponse
    {
         [JsonProperty("current_page")]
         public int CurrentPage { get; set; }
         [JsonProperty("limit")]
         public int Limit{ get; set; }
         [JsonProperty("next_page")]
         public int NextPage{ get; set; }
         [JsonProperty("previous_page")]
         public int PreviousPage{ get; set; }
         [JsonProperty("results")]
         public IEnumerable<JokeResponse> Results{ get; set; }
         [JsonProperty("search_term")]
         public string SearchTerm{ get; set; }
         [JsonProperty("status")]
         public int Status{ get; set; }
         [JsonProperty("total_jokes")]
         public int TotalJokes { get; set; }
         [JsonProperty("total_pages")]
         public int TotalPages { get; set; }
    }
}
