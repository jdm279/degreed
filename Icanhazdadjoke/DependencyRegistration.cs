﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject;
using Icanhazdadjoke.Interfaces;

namespace Icanhazdadjoke
{
    public class DependencyRegistration
    {
        public static void Register(IKernel kernel)
        {
            kernel.Bind<IIcanhazdadjokeRepository>().To<IcanhazdadjokeRepository>();
            kernel.Bind<IRequestData>().To<WebRequestManager>();
        }
    }
}
