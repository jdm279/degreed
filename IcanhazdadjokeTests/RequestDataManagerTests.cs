﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Icanhazdadjoke;
using Icanhazdadjoke.Interfaces;

namespace IcanhazdadjokeTests
{
    [TestClass]
    public class RequestDataManagerTests
    {
        private IRequestData requestData { get; set; }

        [TestInitialize()]
        public void MyTestInitialize()
        {
            requestData = IcanhazdadjokeFactory.GetImplementationForIRequestData();
        }

        [TestCleanup()]
        public void MyTestCleanup()
        {
            requestData = null;
        }

        [TestMethod]
        public void TestInvalidURL()
        {
            try
            {
                requestData.Get("asdf", (wc, func) => func.Invoke());
                Assert.Fail("This is suppose to throw an exception");
            }
            catch (Exception e)
            {
            }
        }
        [TestMethod]
        public void TestValidURL()
        {
            requestData.Get("http://www.google.com", (wc, func) => func.Invoke());
        }
    }
}
