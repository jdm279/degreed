﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Icanhazdadjoke;
using Icanhazdadjoke.Interfaces;
namespace IcanhazdadjokeTests
{
    [TestClass]
    public class DependencyTests
    {

        [TestMethod]
        public void TestDependencyRegistration()
        {
            var kernel = new StandardKernel();
            DependencyRegistration.Register(kernel);
        }
        [TestMethod]
        public void TestIRequestDataRegistration()
        {
            var kernel = new StandardKernel();
            DependencyRegistration.Register(kernel);
            var instance = kernel.Get<IRequestData>(); 
        }
        [TestMethod]
        public void TestIIcanhazdadjokeRepositoryRegistration()
        {
            var kernel = new StandardKernel();
            DependencyRegistration.Register(kernel);
            var instance = kernel.Get<IIcanhazdadjokeRepository>();
        }
    }
}
