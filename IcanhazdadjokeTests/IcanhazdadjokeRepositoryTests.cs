﻿using System;
using System.Collections;
using System.Collections.Generic;
using Icanhazdadjoke;
using Icanhazdadjoke.Interfaces;
using IcanhazdadjokeTests.TestingImplementations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
namespace IcanhazdadjokeTests
{
    [TestClass]
    public class IcanhazdadjokeRepositoryTests
    {
        [TestMethod]
        public void TestGetWithTestData()
        {
            IIcanhazdadjokeRepository repo = IcanhazdadjokeFactory.GetImplementationForIIcanhazdadjokeRepository(new GetRequestDataTester());
            Assert.AreEqual(GetRequestDataTester.GetJokeForGet(), repo.GetRandomJoke());
        }
        [TestMethod]
        public void TestGetWithLiveData()
        {
            IIcanhazdadjokeRepository repo = IcanhazdadjokeFactory.GetImplementationForIIcanhazdadjokeRepository(IcanhazdadjokeFactory.GetImplementationForIRequestData());
            string response = repo.GetRandomJoke();
            Assert.IsTrue(response.Length > 0);
        }
        [TestMethod]
        public void TestSearchWithLiveData()
        {
            IIcanhazdadjokeRepository repo = IcanhazdadjokeFactory.GetImplementationForIIcanhazdadjokeRepository(IcanhazdadjokeFactory.GetImplementationForIRequestData());
            IEnumerable<string> response = repo.SearchJokes(null);
            Assert.AreEqual(30, response.Count());
            response.ToList().ForEach(x => Assert.IsTrue(x.Length > 0));
        }
        [TestMethod]
        public void TestSearchWithLiveDataUsingSearchTerm()
        {
            string term = "Dad Joke";
            IIcanhazdadjokeRepository repo = IcanhazdadjokeFactory.GetImplementationForIIcanhazdadjokeRepository(IcanhazdadjokeFactory.GetImplementationForIRequestData());
            IEnumerable<string> response = repo.SearchJokes(term);
            Assert.IsTrue(response.Count() > 0);
        }
        [TestMethod]
        public void TestSearchWithLiveDataUsingSearchTermWithStrangeCharacters()
        {
            string term = "Dad<>%$;@!";
            IIcanhazdadjokeRepository repo = IcanhazdadjokeFactory.GetImplementationForIIcanhazdadjokeRepository(IcanhazdadjokeFactory.GetImplementationForIRequestData());
            IEnumerable<string> response = repo.SearchJokes(term);
        }
    }
}
