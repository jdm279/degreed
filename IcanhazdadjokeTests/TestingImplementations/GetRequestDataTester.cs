﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Icanhazdadjoke.Interfaces;
using Icanhazdadjoke.POCO;

namespace IcanhazdadjokeTests.TestingImplementations
{
    public class GetRequestDataTester : IRequestData
    {
        public static string GetJokeForGet()
        {
            return "My dog used to chase people on a bike a lot. It got so bad I had to take his bike away.";
        }
        public void Get(string url, Action<WebClient, Func<string>> callback, int retryAttempts = 0)
        {
            using (WebClient w = new WebClient())
            {
                callback.Invoke(w, () =>
                {
                    JokeResponse response = new JokeResponse()
                    {
                        ID = "Test",
                        Joke = GetJokeForGet(),
                        Status = 200
                    };
                    
                    return Newtonsoft.Json.JsonConvert.SerializeObject(response);
                });
            }
        }
    }
}
